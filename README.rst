========
Bio3D Package for Biological Structure Analysis
========

Utilities to analyze, process, organize and explore protein structure, sequence and dynamics data.

Features
========

Features include the ability to read and write structure, sequence and dynamic trajectory data, perform database searches, atom summaries, atom selection, re-orientation, superposition, rigid core identification, clustering, torsion analysis, distance matrix analysis, structure and sequence conservation analysis, normal mode analysis (NMA), and principal component analysis (PCA).  

In addition, various utility functions are provided to enable the statistical and graphical power of the R environment to work with biological sequence and structural data.  Please refer to the main `Bio3D website <http://thegrantlab.org/bio3d/>`_ for more background information.

Contributing to Bio3D
========

We are always interested in adding additional functionality to Bio3D. If you have ideas, suggestions or code that you would like to distribute as part of this package, please contact us (see below). You are also encouraged to contribute your code or issues directly to `this repository <https://bitbucket.org/Grantlab/bio3d/>`_ for incorporation into the development version of the package. For details on how to do this please see the `developer wiki <https://bitbucket.org/Grantlab/bio3d/wiki/Home>`_.  
  
Contact
========

You are welcome to:

* Submit suggestions and bug-reports at: https://bitbucket.org/Grantlab/bio3d/issues
* Send a pull request on: https://bitbucket.org/Grantlab/bio3d/pull-requests
* Compose a friendly e-mail to: bjgrant@umich.edu
