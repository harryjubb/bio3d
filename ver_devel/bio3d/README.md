# Documentation

The Bio3D package consists of <a href="#Input/Output:">input/output</a> functions, <a href="#Sequence Analysis:">sequence analysis</a> functions, <a href="#Structure Analysis:">structure analysis</a> functions, <a href="#Trajectory Analysis:">simulation analysis</a> functions, <a href="#Normal Mode Analysis:">normal mode analysis</a> functions, <a href="#Utilities:">conversion and manipulation</a> functions, and <a href="#Graphics:">graphics</a> functions. Each of these functions is listed below with links to further documentation that includes example code and results. 

Note that you can also get help on any particular function by using the command `?function` or `help(function)` and directly execute the example code for a given function with the command `example(function)` from within R itself. 

We also distribute a number of extended **Bio3D vignettes** that provide worked examples of using Bio3D to perform a particular type of analysis. Currently available vignettes include:
- Installing Bio3D ( <a href="http://thegrantlab.org/bio3d/vignettes/install_vignette/Bio3D_install.pdf">PDF</a> | <a href="http://thegrantlab.org/bio3d/tutorials/installing-bio3d">HTML</a>)
- Getting started with Bio3D ( PDF | <a href="http://thegrantlab.org/bio3d/user-guide">HTML</a> )
- Beginning Trajectory Analysis with Bio3D ( <a href="http://thegrantlab.org/bio3d/vignettes/traj_vignette/Bio3D_md.pdf">PDF</a> | <a href="http://thegrantlab.org/bio3d/tutorials/trajectory-analysis">HTML</a>)
- Enhanced Methods for Normal Mode Analysis with Bio3D ( <a href="http://thegrantlab.org/bio3d/vignettes/nma_vignette/Bio3D_nma.pdf">PDF</a> | <a href="http://thegrantlab.org/bio3d/tutorials/normal-mode-analysis">HTML</a>)
- Comparative sequence and structure analysis with Bio3D ( <a href="http://thegrantlab.org/bio3d/vignettes/pca_vignette/Bio3D_pca.pdf">PDF</a> | <a href="http://thegrantlab.org/bio3d/tutorials/principal-component-analysis">HTML</a>)
- Introduction to correlation network analysis with Bio3D ( PDF | HTML)

There is also a <a href="http://thegrantlab.org/bio3d/vignettes/bio3d.pdf">package manual</a> (in PDF format) that is a concatenation of each functions documentation. 

Note that for information on Bio3D development status or to report a bug, please refer to: https://bitbucket.org/Grantlab/bio3d 
