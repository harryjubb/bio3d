print.pdb <- function(x, ...) {
  ## Print a summary of basic PDB object features
  y <- summary.pdb(x, printseq=TRUE, ...)
}
